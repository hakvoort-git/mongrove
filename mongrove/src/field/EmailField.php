<?php

namespace mongrove;

/**
 *
 * The email field represents a field containing a string email value.
 *
 * @author Gido Hakvoort <gido@hakvoort.it>
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
class EmailField extends SimpleField {

    /**
     * Define a new EmailField
     */
    public function __construct() {
        parent :: __construct();
    }

    /**
     * (non-PHPdoc)
     * @see src/mongrove.SimpleField::setValueImpl()
     */
    protected function setValueImpl($value) {
        if($this->value === $value) {
            return false;
        }

        if(filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            throw new \Exception("'{$value}' is not a valid email address");
        }

        $this->value = $value;

        return true;
    }
}