<?php

namespace mongrove;

/**
 * Mongo query/update/insert keywords.
 *
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 * @author Gido Hakvoort <gido@hakvoort.it>
 *
 */
interface Command {

    /*
     * Modification operators
     */
    const OP_INC       = '$inc';
    const OP_SET       = '$set';
    const OP_UNSET     = '$unset';
    const OP_PUSH      = '$push';
    const OP_PUSH_ALL  = '$pushAll';
    const OP_POP       = '$pop';
    const OP_PULL      = '$pull';
    const OP_PULL_ALL  = '$pullAll';

    /*
     * Conditional operators
     */
    const CON_OP_LESS               = '$lt';
    const CON_OP_GREATER            = '$gt';
    const CON_OP_LESS_OR_EQUAL      = '$lte';
    const CON_OP_GREATER_OR_EQUAL   = '$gte';
    const CON_OP_NOT_EQUAL          = '$ne';
    const CON_OP_IN                 = '$in';
    const CON_OP_NOT_IN             = '$nin';
    const CON_OP_ALL_IN             = '$all';
    const CON_OP_SIZE               = '$size';
    const CON_OP_EXISTS             = '$exists';
    const CON_OP_TYPE               = '$type';

    /*
     *  Non-existing operators, but used for simplifying query transformations
     */
    const CON_OP_EQUAL              = '$eq';
    const CON_OP_LIKE               = '$regex';
}