<?php

namespace mongrove;

/**
 *
 * The CollectionManager is a central entity responsible for resolving
 * Collections by Record type, Mongo collection name and Mongo database
 * name.
 *
 * @author Gido Hakvoort <gido@hakvoort.it>
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
class CollectionManager {

    protected $dbManager = null;
    protected $collections = array();

    private static $provider = null;

    /**
     * Create a new CollectionManager with the backing
     * DatabaseManager for retrieving database handles.
     *
     * @param DatabaseManager $dbManager The backing DatabaseManager
     */
    public function __construct(\mongrove\DatabaseManager $dbManager) {
        $this->dbManager = $dbManager;
    }

    /**
     * Get the DatabaseManager used by the CollectionManager.
     *
     * @return \mongrove\DatabaseManager The internally used DatabaseManager
     */
    public function getDatabaseManager() {
        $this->dbManager;
    }

    /**
     * Resolve a Collection for the given Record type, Mongo collection name
     * and Mongo database name.
     *
     * @param string $type
     * @param string $collectionName
     * @param string $databaseName
     *
     * @return \mongrove\Collection
     */
    public function getCollection($type, $collectionName, $databaseName) {
        if(!isset($this->collections[$collectionName])) {
            $database = $this->dbManager->getDatabase($collectionName, $databaseName);

            $this->collections[$type] = new Collection($type, $collectionName, $database);
        }

        return $this->collections[$type];
    }

    /**
     * Resolve the CollectionManager from the set
     * CollectionProvider.
     *
     * @return \mongrove\CollectionManager
     */
    public static function getCollectionManager() {
        return self :: $provider->getCollectionManager();
    }

    /**
     * Set the CollectionManagerProvider responsible for creating an instance
     * of the CollectionManager.
     *
     * @param \mongrove\CollectionManagerProvider $provider
     */
    public static function setCollectionManagerProvider(CollectionManagerProvider $provider) {
        self :: $provider = $provider;
    }
}

/**
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 * @author Gido Hakvoort <gido@hakvoort.it>
 *
 *
 */
interface CollectionManagerProvider {

    /**
     * @return \mongrove\CollectionManager
     */
    public function getCollectionManager();
}

/**
 *
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 * @author Gido Hakvoort <gido@hakvoort.it>
 *
 */
class MangroveCollectionManagerProvider implements CollectionManagerProvider {

    /**
     * (non-PHPdoc)
     * @see src/mongrove.CollectionManagerProvider::getCollectionManager()
     */
    public function getCollectionManager() {
        /*
         * Refer to Mangrove's dependency injection
         */
        return get('mongrove\\CollectionManager');
    }

}

/**
 *
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 * @author Gido Hakvoort <gido@hakvoort.it>
 *
 */
class SimpleCollectionManagerProvider implements CollectionManagerProvider {

    protected $collectionManager = null;

    /**
     *
     * Enter description here ...
     * @param $collectionManager
     */
    public function __construct(CollectionManager $collectionManager) {
        $this->collectionManager = $collectionManager;
    }

    /**
     * (non-PHPdoc)
     * @see src/mongrove.CollectionManagerProvider::getCollectionManager()
     */
    public function getCollectionManager() {
        return $this->collectionManager;
    }
}

/*
 *  Use Mangrove if available
 */
if(defined('MG_VERSION')) {
    CollectionManager::setCollectionManagerProvider(new MangroveCollectionManagerProvider());
}



