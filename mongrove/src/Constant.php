<?php

namespace mongrove;

/**
 *
 * Mongo and Mongrove special keywords.
 *
 * @author Gido Hakvoort <gido@hakvoort.it>
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
interface Constant {

    const ID               = '_id';
    const TYPES            = '_types';
    const STRUCTURE        = '_structure';

    const REF_ID           = '$id';
    const REF_COLLECTION   = '$ref';
    const REF_TYPE         = '$type';

    const OPTION_QUERY     = '$query';
    const OPTION_ORDERBY   = '$orderby';
    const OPTION_EXPLAIN   = '$explain';
    const OPTION_HINT      = '$hint';
}