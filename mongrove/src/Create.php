<?php

namespace mongrove;

/**
 *
 * A helper class for chaining methods in Structure definition.
 *
 * @author Gido Hakvoort <gido@hakvoort.it>
 * @author Michiel Hakvoort <michiel@hakvoort.it>
 *
 */
final class create {

    private function __construct() {

    }

    /**
     *
     * @return \mongrove\Structure
     */
    public static function Structure() {
        return new Structure();

    }
    /**
     *
     * @return \mongrove\StructureField
     */
    public static function StructureField(Structure $structure = null) {
        return new StructureField($structure ?: new Structure());
    }

    /**
     *
     * @return \mongrove\StringField
     */
    public static function StringField($default = null) {
        return new StringField($default);
    }

    /**
     *
     * @return \mongrove\AtomicIntegerField
     */
    public static function AtomicIntegerField($default = 0) {
        return new AtomicIntegerField($default);
    }

    /**
     *
     * @return \mongrove\IntegerField
     */
    public static function IntegerField($default = null) {
        return new IntegerField($default);
    }

    /**
     *
     * @return \mongrove\EnumField
     */
    public static function EnumField(array $enums = array(), $default = null) {
        return new EnumField($enums, $default);
    }

    /**
     *
     * @return \mongrove\EmailField
     */
    public static function EmailField() {
        return new EmailField();
    }

    /**
     *
     * @return \mongrove\ListField
     */
    public static function ListField(array $default = array()) {
        return new ListField($default);
    }

    /**
     *
     * @return \mongrove\MapField
     */
    public static function MapField(array $default = array()) {
        return new MapField($default);
    }

    /**
     *
     * @return \mongrove\DateField
     */
    public static function DateField($default = null) {
        return new DateField($default);
    }

    /**
     *
     * @return \mongrove\LocationField
     */
    public static function LocationField($default = null) {
        return new LocationField($default);
    }

    /**
     *
     * @return \mongrove\ReferenceField
     */
    public static function ReferenceField($default = null) {
        return new ReferenceField($default);
    }
}

